
/*

    This directive creates the HTML that will be rendered onto the DOM.
    The important part here is that we are calculating a unique ID named divId
    that will be set as the HTML element's ID attribute.  When your custom 
    visualization is set to be rendered, you can search for the container element
    that matches $('#' + divId)
    
*/

mod.directive(settings.widgetName, [

    function ($timeout, $dom) {

        //  Get the name of the plugin widget, from config.js
        var widgetName = settings.widgetName;

        //  Render the widget's template, passing in a unique identifier for this div
        return {
            priority: 0,
            replace: false,
            templateUrl: "/plugins/" + widgetName + "/widget/template.html",
            transclude: false,
            restrict: 'E',
            link: function ($scope, element, attrs) {

                /*********************************************************/
                /*   Business Logic to run before the HTML is rendered   */
                /*********************************************************/

                //  Generate an ID for the div container
                $scope.divid = settings.widgetName + '-' + $$get($scope, 'widget.oid','');

                //  Wait until template has loaded
                setTimeout(function(){

                    /*********************************************************/
                    /*  Business Logic to run after the HTML is rendered    */
                    /*********************************************************/
                    
                },0)

            }
        }
    }]);

