
//	Supporting Sisense services
var dateFormatter = prism.$injector.get('$filter')('date'),
    numberFormatter = prism.$injector.get('$filter')('numeric');

/**********************************************************/
/*	Functions to options are available for subqueries     */
/**********************************************************/

var subqueries = [
	{
		label: "Display Values",
		multiColumn: true,
		processResult: function(queryResponse, baseResults){

			//	Understand the metadata used for this query
			var metadata = parseMetadata(queryResponse.metadata);

			//	Consolidate the data, to match the dimensionality of the rows
			var subquery = parseSubqueryData(queryResponse.values, metadata);

			//	Create a results data frame to send back
			var results = [];

			//	Loop through the base results
			baseResults.data.forEach(function(row, rowNumber){

				//	Get the key for this row
				var key = getKey(row, metadata.dimensions,0);

				//	Look for a match in the subquery
				var data = $$get(subquery.data, key, {});

				//	Insert it into the base result's data frame 
				results[rowNumber] = data;
			})

			return {
				data: results,
				columns: subquery.columnHeaders,
				stats: subquery.stats
			};

		},
		ready: function(widget, columnId){

		}
	}
]

//	Function to consolidate the subquery into dimensions that match the base table
function parseSubqueryData(data, metadata){

	//	Get the indexes of where to find data in the resultset
	var measureIndex = metadata.dimensions.length + metadata.subQueryDimensions.length;

	//	Find the metadata info for the measure
	var measureMetadata = $$get(metadata, 'measures.0',{});					//	Handles only a single measure...should be fine
	var mask = $$get(measureMetadata,'format.mask', {});
	var colorType = $$get(measureMetadata, 'format.color.type', 'default'); //	Either single or conditional coloring

	//	Create an object to hold all relevant information
	var result = {
		data: {},
		dataTable: [],
		rows: {},
		columns: {},
		columnHeaders: [],
		stats: {
			rowCount: 0,
			columnCount: 0,
			cellCount: data.length,
			subQueryDimensions: metadata.subQueryDimensions.length
		}
	}

	//	Loop through each row in the dataset, and create a skeleton of the dataframe
	for (var i=0; i<data.length; i++){

		//	Calculate the unique identifiers for this row (table dimensions + subquery dimensions)
		var rowKey = getKey(data[i], metadata.dimensions, 0),
			columnKey = getKey(data[i], metadata.subQueryDimensions, metadata.dimensions.length);

		//	Check to see if the keys already exist
		var rowExists = $$get(result.rows, rowKey, false),
			columnExists = $$get(result.columns, columnKey, false);

		//	Create a new entry for the table, if it doesn't exist
		if (!rowExists){

			//	Store the row number
			result.rows[rowKey] = {
				index: result.stats.rowCount
			};

			//	Increment the counter
			result.stats.rowCount += 1;

			//	Create an empty array to hold this row
			result.dataTable.push([]);
			result.data[rowKey] = [];
		}	

		//	Create a new entry for the subquery, if it doesn't exist
		if (!columnExists){

			//	Store the column number 
			result.columns[columnKey] = {
				index: result.stats.columnCount,
				labels: {}
			};

			//	Increment the counter
			result.stats.columnCount += 1;
		}
	}

	//	Handle when we get no rows from a query
	if (result.dataTable.length == 0){
		result.dataTable.push([])
		result.data[settings.singleRowId] = []
	}

	//	Loop through the dataset again, and store the data as a table
	for (var j=0; j<data.length; j++){

		//	Calculate the unique identifiers for this row (table dimensions + subquery dimensions)
		var rowKey = getKey(data[j], metadata.dimensions, 0),
			columnKey = getKey(data[j], metadata.subQueryDimensions, metadata.dimensions.length);

		//	Figure out where in the table, this data point lives
		var rowNumber = $$get(result.rows, rowKey + '.index', 0),
			columnNumber = $$get(result.columns, columnKey + '.index', 0);

		//	Get the data point
		var point = data[j][measureIndex];
		
		//	Create the formatted text
		point.text = numberFormatter(point.data, mask);

		//	Add subquery dimensions to the point
		point.dimensions = [];
		point.dimensionLabel = '';

		//	Loop through all possible dimensions
		metadata.subQueryDimensions.forEach(function(dim, index){

			//	Check for a date level
			var dateLevel = $$get(dim,'jaql.level', null);

			//	Get the text of this dimension
			var value = data[j][dim.index].text,
				formattedValue = dateLevel ? dateFormatter(data[j][dim.index].data, getDateMask(dateLevel)) : value;

			//	Calculate the dimension's point
			var dValue = {
				data: value,
				text: formattedValue
			}

			//	Save to the point
			point.dimensions.push(dValue);
			point.dimensionLabel += value + settings.separator;

			//	Save a reference to the formatted version of this dimension
			if (!result.columns[columnKey].labels[value]){
				result.columns[columnKey].labels[value] = formattedValue;
			}
		})

		//	Save the point into the table
		result.dataTable[rowNumber][columnNumber] = point;
		result.data[rowKey][columnNumber] = point;
	}

	//	Loop through the dataset one more time, to ensure all rows have the right # of columns
	for (id in result.data) {

        //  Loop through each cell in the row
        var row = result.data[id];
        for (var c=0; c<result.stats.columnCount; c++){
            
            //  Is there an object at this location?
            if (!row[c]){

                //  No row, must be a null value
                row[c] = {
                    text: settings.nullLabel,
                    data: null,
                    isNull: true
                }
            }
        }
    }

	//	Loop through the columns
	for (key in result.columns){

		//	Get the column # for this key
		var myColumn = $$get(result, 'columns.' + key, -1);
		if (myColumn.index >= 0){

			//	Parse our the dimension text, including the measure title as the top level
			result.columnHeaders[myColumn.index] = [measureMetadata.jaql.title];
			key.split(settings.separator).forEach(function(sKey){
				result.columnHeaders[myColumn.index].push(result.columns[key].labels[sKey]);
			})
		}
	}

	return result;
}

//	Function to determine the unique key per row
function getKey(row, dimensions, offset){

	//	Were there no row's specified?
	if (dimensions.length == 0) {

		//	Just use a generic id
		return settings.singleRowId;

	} else {

		//	Init the key string
		var key = [];

		//	Loop through each dimension, and append to the key
		for (var j=0; j<dimensions.length; j++){

			//	Get the raw cell
			var cell = $$get(row, (j+offset), '');

			//	Figure out if this is a date or any other field
			var isDate = (dimensions[j].jaql.datatype == "datetime");
			key[j] = isDate ? moment(cell.data).unix() : cell.text;
		}

		return key.join(settings.separator);
	}
}

//	Function to parse the metadata
function parseMetadata(metadata){

	//	Get the raw data + metadata
	var results = {
			dimensions: [],
			subQueryDimensions: [],
			measures: []
		};

	//	Loop through each metadata item, and categorize it
	metadata.forEach(function(item, index){ 
		
		//	Evaluate this item
		var isSubqueryDim = item.jaql[settings.subquery.dimensionProperty],
			isMeasure = item.jaql.formula || item.jaql.agg;
		
		//	Save the index within the item
		item.index = index;

		//	Figure out the grouping of the dimension
		if (isSubqueryDim){
			results.subQueryDimensions.push(item);
		} else if (isMeasure){
			results.measures.push(item);
		} else {
			results.dimensions.push(item)
		}
	})

	return results;
}

//	Function to build a formatting mask, depending on a level
function getDateMask(level){

	if (level == 'years') {
		return 'yyyy';
	} else if (level == 'quarters') {
		return 'Q yyyy';
	} else if (level == 'months') {
		return 'MMM yyyy';
	}  else if (level == 'weeks') {
		return 'MMM d, yyyy';
	} else if (level == 'days') {
		return 'MMM d, yyyy';
	} else {
		return null;
	}
}

/**********************************************************/
/*	Functions to add the new dropdown menu for subqueries */
/**********************************************************/

prism.run([function() {

	//  Function to make sure this is a settings menu for a measure
    function isSettingsMenu(args) {
        
        //  Try to evaluate
        try {

        	//	Must be in the widget editor
			var widgetEditorOpen = (prism.$ngscope.appstate === "widget");
			if (!widgetEditorOpen){
				return false
			}

            // Widget must be an allowed chart            
            if(prism.$ngscope.widget.manifest.name !== settings.widgetName ){
                return false;
            }

            //	Make sure the user clicked on a metadata item menu
            var isMetadataItemMenu = (args.settings.name === "widget-metadataitem");
            if (!isMetadataItemMenu){
            	return false;
            }

        } catch(e) {
            return false;
        }

        return true;
    }

    //	Function to add the widget menu item
    function addMenuItem(event, args){

		//	Check for metadata menu
		var metadataMenu = isSettingsMenu(args,'metadataMenu');
		if (metadataMenu) {
			
			//	Get the widget object
			var widget = event.currentScope.widget;

			//	Get the metadata item that was clicked
			var item = args.settings.item,
				panelItems = args.settings.item.$$panel.items;

			//  Create a separator
	        var separator = {
	            type: "separator"
	        };

			//	Create settings menu
			var options = {
				caption: settings.subquery.label,
				items:[]
			};

			//	Loop through each available function
			subqueries.forEach(function(func){

				//	Make sure the function object is valid
				var isValid = (func.label && func.processResult);
				if (isValid){

					//	Look for existing settings
					var currentSelection = $$get(item, 'jaql.' + settings.subquery.propName, null);

					//	Create the base menu item
					var subItem = {
						caption: func.label,
						checked: currentSelection === func.label,
						closing: false,
						disabled: false,
						size: "xl",
						type: "check",
						widget : widget,
						functionObj: func,
						item: item,
						panelItems: panelItems,
						execute: metadataItemClicked
					}

					//	Add header item
					options.items.push(subItem);
				}
			})
			
			//	Add options to the menu
			args.settings.items.push(separator);
			args.settings.items.push(options);	
		}
    }

    //	Event handler for when the metadata item menu was clicked
	function metadataItemClicked(){

		//	Get the name of the property
		var propName = settings.subquery.propName;

    	//	Get a reference to the widget's settings
    	var currentSelection = this.item.jaql[propName],
    		caption = this.caption;

    	//	was this property selected before?
    	var existing = (currentSelection == caption);

    	//	Update the settings of the item
    	if (existing){
    		delete this.item.jaql[propName]
    	} else {
    		this.item.jaql[propName] = caption;
    	}

		//	Redraw the widget
		this.widget.refresh();
	}

	//	Add event handler
	prism.on("beforemenu", addMenuItem);

}])