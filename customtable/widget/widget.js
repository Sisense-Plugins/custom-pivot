
//  Panel name defintion
var panels = {
    "rows": "Rows",
    "values": "Values",
    "filters":"filters"
}

//  Get a reference to the Sisense date/number formatters
var dateFormatter = prism.$injector.get('$filter')('date'),
    numberFormatter = prism.$injector.get('$filter')('numeric'),
    $http = prism.$injector.get('$http');

//  New Chart type registration
prism.registerWidget(settings.widgetName, {
    name: settings.widgetName,
    family: "map",
    title: "Custom Table",
    iconSmall: "/plugins/" + settings.widgetName + "/resources/icon-24.png",
    styleEditorTemplate: "/plugins/" + settings.widgetName + "/designPanel/placeholder.html",
    hideNoResults: true,
    style: {
        transponse: false,
        maxPoints: 10000
    },
    directive: {
        desktop: settings.widgetName
    },
    data: {
        selection: [],
        defaultQueryResult: {},
        panels: [
            {
                name: panels.rows,
                type: "visible",
                metadata: {
                    types: ['dimensions'],
                    maxitems: 10
                },
                itemAttributes: ["color"],
                allowedColoringTypes: function(){
                    return {
                        color: true,
                        condition: false,
                        range: true
                    }
                },
                visibility: true
            },
            {
                name: panels.values,
                type: "visible",
                metadata: {
                    types: ['measures'],
                    maxitems: 10
                },
                itemAttributes: ["color"],
                allowedColoringTypes: function(){
                    return {
                        color: true,
                        condition: true,
                        range: true
                    }
                },
                visibility: true
            },
            {
                name: 'filters',
                type: 'filters',
                metadata: {
                    types: ['dimensions'],
                    maxitems: -1
                }
            }
        ],
        //  Allow coloring for the value panel
        canColor: function (widget, panel, item) {
            return (panel.name === panels.values);
        },
        // builds a jaql query from the given widget
        buildQuery: function (widget) {

            // building jaql query object from widget metadata 
            var query = { 
                datasource: widget.datasource, 
                format: "json",
                isMaskedResult:true,
                offset: 0,
                count: widget.style.maxPoints,
                metadata: [] 
            };

            //  Define the number of queries to run for this widget
            widget.options.queryCount = 1;
            widget.options.queriesComplete = 0;
            widget.options.subqueries = [];

            /*  Add the dimenions to the query */
            widget.metadata.panel(panels.rows).items.forEach(function(item, index){

                //  Create a copy of the metadata item
                var newItem = $$.object.clone(item, true);

                //  Set the column index
                newItem.jaql.index = index;

                //  Define the index
                query.metadata.push(newItem);
            })

            //  Get the number of dimension items
            var numDims = widget.metadata.panel(panels.rows).items.length;

            //  Get the metadata items from the widget's filters panel
            widget.metadata.panel(panels.filters).items.forEach(function(item){

                //  Create a copy of the metadata item
                var newItem = $$.object.clone(item, true);

                //  Since this is a filter, add a scoping property (so jaql knows it a filter)
                newItem.panel = "scope";

                //  Save this to the metadata
                query.metadata.push(newItem);
            })

            //  Get the number of dimension items
            var numFilters = widget.metadata.panel(panels.filters).items.length;

            //  Loop through all the values
            widget.metadata.panel(panels.values).items.forEach(function(item, index){

                //  Check to see if this measure contains a subquery
                var isSubQuery = item.jaql[settings.subquery.propName];
                if (isSubQuery){

                    //  Create a copy of the query
                    var newQuery = $$.object.clone(query,true);

                    //  Make sure to only include the dimensions & filters
                    newQuery.metadata = query.metadata.filter(function(oldItem){
                        return oldItem.$$panel.name !== panels.values;
                    })

                    //  Parse the formula of the current item, and add it to the query
                    parseSubquery(item).forEach(function(sItem){
                        newQuery.metadata.push(sItem);
                    })

                    //  Add dashboard filters
                    var newQuery = addDashboardFilters(newQuery);

                    //  Make separate API call for this query
                    makeSubquery(widget, newQuery, numDims + index)

                    //  Increment the query counter
                    widget.options.queryCount += 1;
                } else {

                    //  Create a copy of the metadata item
                    var newItem = $$.object.clone(item, true);

                    //  Mark the column index
                    newItem.jaql.index = numDims + index;

                    //  combine simple values into the base query
                    query.metadata.push(newItem)
                }
            })

            return query;
        },
        //  Build geojson object from query result
        processResult : function (widget, queryResult) {

            //  raw data to work with
            var rawData = queryResult.$$rows,
                metadata = queryResult.metadata(),
                hasData = rawData.length > 0,
                colors = prism.$ngscope.dashboard.style.options.palette.colors;

            //  Define an object to hold the new results
            var newResults = {
                data: [],
                headers: [],
                stats: {}
            };

            //  Loop through each row to create the data table
            for (var i=0; i<rawData.length; i++){

                //  Get the row data
                var dataRow = rawData[i];

                //  Setup a new row, that puts the columns in the proper space
                var row = [];

                //  Loop through the existing data, and put each cell in the right index
                for (var j=0; j<dataRow.length; j++){

                    //  Get the index from the metadata, and save the cell
                    var properIndex = $$get(metadata[j], 'jaql.index',j);
                    row[properIndex] = dataRow[j];
                }

                //  Save the row to the data table
                newResults.data.push(row);
            }

            //  Loop through each metadata item (columns), and save the column name
            metadata.forEach(function(item, idx){
                newResults.headers[item.jaql.index] = [item.jaql.title];
                
            })

            //  Save the stats about this query
            newResults.stats = {
                "rowCount": rawData.length,
                "dimensionCount": widget.metadata.panel(panels.rows).items.length,
                "columnLevels": 1
            }

            //  Increment the queries complete counter
            widget.options.queriesComplete += 1;
            console.log("CustomTable: Base Query Complete");

            //  if we have data, return the map data
            return hasData ? newResults : null;
        }
    },
    render : function (widget, event) {
        //  Run the rendering function
        tryRender(widget);
    },
    destroy : function (widget, args) {},
    options: {
        dashboardFiltersMode: "select",
        selector: false,
        title: false
    },
    sizing: {
        minHeight: 128, //header
        maxHeight: 2048,
        minWidth: 128,
        maxWidth: 2048,
        height: 320,
        defaultWidth: 512
    }
});


/************************************************************/
/*  Define custom functions for supporting multiple queries */
/************************************************************/

//  Function to parse a subquery metadata item, and build appropriate jaql
function parseSubquery(item) {

    //  Convert this formula to a set of dimensions and measures
    var metadataItems = [],
        subDimensions = [],
        subMeasures = [],
        contexts = item.jaql.formula.match(/\[.*?\]/g);

    //  Get the subquery type
    var subqueryType = item.jaql[settings.subquery.propName];

    //  Loop through each part of the formula
    contexts.forEach(function(id){

        //  Make sure the context exists
        if (item.jaql.context.hasOwnProperty(id)){

            //  Create a jaql metadata item from the context
            var context = {
                jaql: item.jaql.context[id]
            };

            if (context.jaql.agg || context.jaql.formula){

                //  This item is a measure, so make sure to include the formatting (for colors) & subquery type
                context.format = item.format;
                context.jaql[settings.subquery.propName] = subqueryType;
                context.jaql.title = item.jaql.title;

                //  Save to the submeasures array
                subMeasures.push(context);
            } else {

                //  This item is a dimension, just save it as is
                context.jaql[settings.subquery.dimensionProperty] = true;
                subDimensions.push(context);
            }
        }
    })

    //  Add the sub dimensions
    subDimensions.forEach(function(item){
        metadataItems.push(item);
    })              

    //  Add the sub measures
    subMeasures.forEach(function(item){
        metadataItems.push(item);
    })  

    //  Check for conditional coloring, and add the conditions to the query
    var conditions = $$get(item, 'format.color.conditions', []);
    conditions.forEach(function(condition, i){

        //  Define the base metadata item, with additional attributes
        var colorJaql = {
            'highlevel': true,
            'operator': condition.operator,
            'color': condition.color,
            'index': i,
            'jaql': {}
        }

        //  What kind of condition is it?
        if (typeof condition.expression !== 'string') {

            //  calculated condition
            colorJaql.jaql = condition.expression.jaql
            
        } else {

            //return

            //  static condition
            colorJaql.jaql = {
                'type': 'measure',
                'formula': condition.expression
            }
        }

        //  Append to query
        metadataItems.push(colorJaql);
    })

    //  Return the new jaql
    return metadataItems;
}

//  Function to add dashboard filters to the query
function addDashboardFilters(query){

    //  Get the list of dashboard filters, flattening out any cascading filters
    var dashboardFilters = [];
    prism.$ngscope.dashboard.filters.$$items.forEach(function(filter){

        if (!filter.disabled) {
            //  Check to see if the filter is cascading (dependant)
            if (filter.isCascading) {
                //  Need to loop through each level, and push the filter
                filter.levels.forEach(function(levelFilter){
                    dashboardFilters.push(levelFilter)
                })
            } else {
                //  Regular filter, just add the jaql
                dashboardFilters.push(filter.jaql)
            }
        }
    })

    dashboardFilters.forEach(function(filter){

        //  Evaluate this filter
        var isDateDim = (filter.datatype == "datetime");

        //  Look for any matching filters @ the widget level
        var matchingItems = query.metadata.filter(function(item){ 
            var isMatch = (item.jaql.table == filter.table) && (item.jaql.column == filter.column),
                dateCheck = !isDateDim ? true : (item.jaql.level == filter.level);
            return isMatch && dateCheck;
        })

        //  Is this metadata item part of the existing query?
        if (matchingItems.length > 0){

            //  Find the matching item, and check if its a filter
            var matchItem = matchingItems[0],
                isWidgetFilter = (matchItem.panel == "scope");

            //  Only do something if the match is not a widget filter
            var filterCriteria = isWidgetFilter ? null : filter.filter;
            if (filterCriteria){
                matchItem.jaql.filter = filterCriteria;
            }
        } else {

            //  Totally separate filter, so add it to the query
            query.metadata.push({
                'jaql': filter,
                'panel': "scope"
            })
        }
    })

    return query;
}

//  Function for running subqueries for the widget
function makeSubquery(widget, subquery, columnIndex){

    //  Build the query URL
    var elasticube = (typeof subquery.datasource == "string") ? subquery.datasource : subquery.datasource.title;
    var url = '/api/datasources/' + elasticube + '/jaql';

    //  Make the API call for the subquery
    $http.post(url, subquery).then(function(response){

        //  Save the data from this subquery, back to the widget
        widget.options.subqueries[columnIndex] = response.data;
        widget.options.queriesComplete += 1;
        console.log("CustomTable: Subquery Complete");

        //  Try to render the widget now
        tryRender(widget)
    })
}

//  Function to see if we have all the data to render
function tryRender(widget){

    console.log("CustomTable: Trying to render");

    //  Did all the queries complete?
    var isReady = (widget.options.queryCount === widget.options.queriesComplete);
    if (isReady) {

        //  process the full data set
        var result = myProcessor(widget, widget.queryResult, widget.options.subqueries);

        //  Render the results
        myRenderer(widget, result);
    } 
}

//  Function to consolidate the base query + subqueries into a single table
function myProcessor(widget, mainQueryResult, subQueryResults){

    //  Create a clone of the original query result, to work with
    var queryResult = $$.object.clone(mainQueryResult, true);

    var columnOffset = 0;

    //  Loop through each subquery
    subQueryResults.forEach(function(result, columnIndex){

        //  The subqueries are positioned to match up w/ the column in the main table,
        //  so its expected to have empty columns in the subquery results.  Therefore
        //  we need to check that result at this position has some data
        if(result){

            //  Get the measure, which will be the last item in the jaql result
            var measure = result.metadata[result.metadata.length-1];
            if (measure){

                //  Get the type of subquery that was run
                var subqueryType = $$get(measure, 'jaql.subquery', null);

                //  Lookup the function assosiated with this subquery
                var matchingFunctions = subqueries.filter(function(s){
                    return s.label == subqueryType;
                })
                var func = $$get(matchingFunctions, '0', null);
                if (func) {

                    //  Let the subquery handler process the results
                    var processResult = func.processResult(result, queryResult);

                    //  How are we displaying this subquery?
                    if (func.multiColumn) {

                        //  Add the column dimensions
                        //queryResult.headers.splice(columnIndex+idx,deleteColumns,column.dimensions);
                        processResult.columns.forEach(function(columnTitle, idx){

                            //  Figure out if we should delete any items
                            var deleteColumns = (idx == 0) ? 1 : 0;

                            //  Splice the data in
                            queryResult.headers.splice(columnIndex+columnOffset+idx,deleteColumns,columnTitle);
                        })

                        //  Increase the number of column levels, based on how many dimensions are in the subquery
                        queryResult.stats.columnLevels = processResult.stats.subQueryDimensions + 1;

                        //  Loop through each row in the result set
                        queryResult.data.forEach(function(row, rowNumber){

                            //  Need to insert multiple columns, insert the array of 
                            //  results for this row while removing the placeholder spot
                            processResult.data[rowNumber].forEach(function(column,idx){

                                //  Figure out if we should delete any items
                                var deleteColumns = (idx == 0) ? 1 : 0;

                                //  Splice the data in
                                row.splice(columnIndex+columnOffset+idx,deleteColumns,column);
                            })
                        })

                        //  Add to the column offset
                        columnOffset += processResult.columns.length;
                    } else {

                        //  Add the column header for this single column
                        queryResult.headers[columnIndex+columnOffset] = $$(measure, 'jaql.title', '');

                        //  Loop through each row in the result set
                        queryResult.data.forEach(function(row, rowNumber){

                            //  the data was consolidated to a single column
                            row[columnIndex+columnOffset] = processResult.data[rowNumber]
                        })
                    }
                }
            }
        }
    })

    //  Return the new table of data to work with
    return cleanData(queryResult, widget.metadata);
}

//  Function to add null padding in the dataset
function cleanData(queryResult, metadata){

    //  Loop through each row in the table
    for (var r=0; r<queryResult.data.length; r++){

        //  Loop through each cell in the row
        var row = queryResult.data[r]
        for (var c=0; c<queryResult.data[0].length; c++){
            
            //  Is there an object at this location?
            if (!row[c]){

                //  No row, must be a null value
                row[c] = {
                    text: settings.nullLabel,
                    data: null,
                    isNull: true
                }
            }
        }
    }

    return queryResult;
}

//  Function to finally render the table
function myRenderer(widget, data){

    //  Find the HTML container element
    var divId = settings.widgetName + '-' + $$get(widget, 'oid',''),
        element = $('#' + divId);

    //  Create the HTML table
    var html = widget.style.transponse ? tableBulderTranspose(data) : tableBuilder(data);

    //  Write the HTML to the dom
    element.empty();
    element.append(html);

    //  Trigger dom-ready event, for pdf printing
    //widget.trigger('domready');

    console.log("CustomTable: Rendering Complete");
}

function tableBuilder(results){

    //  Create the outline of the table
    var table = '<table>';

    //  Create the headers row
    for (var i=0; i<results.stats.columnLevels; i++){

        //  Create the row
        var headerRow = '<tr class="customTable-headersRow">';
        for (var j=0; j<results.headers.length; j++){

            //  Get the text to display in this cell
            var lastCellText = $$get(results.headers, (j-1)+'.'+i, ''),
                thisCellText = $$get(results.headers, j+'.'+i, ''),
                cellText = (thisCellText == lastCellText) ? '' : thisCellText;

            //  Create the header cell as HTML
            headerRow += '<td class="customTable-headersCell" columnIndex="' + j + '">' + cellText + '</td>';
        }
        headerRow += '</tr>';

        //  Add to the table
        table += headerRow;
    }

    //  Loop through each data row
    results.data.forEach(function(row, rowNumber){

        //  Create the row's HTML
        var dataRow = '<tr class="customTable-dataRow">';
        row.forEach(function(cell, columnNumber){
            dataRow += '<td class="customTable-dataCell" value="' + cell.data + '">' + cell.text + '</td>';
        })
        dataRow += '</tr>';

        //  Add to the table
        table += dataRow;
    })

    //  Finish the table
    table += '</table>';
    return table;
}

function tableBulderTranspose(data){

}